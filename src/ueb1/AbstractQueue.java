package ueb1;

/**
 * @author falx
 * @version v0.1a
 */
public abstract class AbstractQueue
{
    protected Object[] queue;
    protected int headptr;
    protected int tailptr;
    protected int fill;

    protected AbstractQueue( int size )
    {
        if( size <= 0 )
            throw new IllegalArgumentException( "size must be greater 0!" );
        this.queue = new Object[size];
        this.headptr = -1;
        this.tailptr = 0;
    }


    public int getSize()
    {
        return queue.length;
    }


    public Object dequeue()
    {
       if( headptr != -1 )
       {
           Object o = queue[headptr];
           headptr = ++headptr % queue.length;
           fill--;
           if( fill == 0 )
               headptr = -1;
           return o;
       }
       else
           return null;
    }

    public abstract boolean enqueue( Object o );

    public abstract int getFreeSpace();
}
