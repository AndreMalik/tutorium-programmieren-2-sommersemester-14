package ueb1;

/**
 * @author falx
 * @version v0.1a
 */
public class Stack implements IStack
{
    private int stackptr;
    private Object[] stack;


    public Stack( int size )
    {
        this.stack = new Object[size];
        this.stackptr = 0;
    }



    @Override
    public void push( Object element ) throws StackFullException
    {
        if( stackptr == stack.length )
            throw new StackFullException();

        stack[stackptr++] = element;
        /*
        stack[stackptr] = element;
        stackptr++;
         */
    }

    @Override
    public Object pop()
    {
        if( stackptr == 0 )
            return null;

        return stack[--stackptr];
    }

    @Override
    public void increaseSize( int newSize ) throws IllegalArgumentException
    {
        if( newSize < stack.length )
            throw new IllegalArgumentException( "newSize must be greater than the current stack size" );

        Object[] newStack = new Object[newSize];
        for( int i = 0; i < stackptr; i++ )
            newStack[i] = stack[i];

        stack = newStack;
    }

    @Override
    public int getSize()
    {
        return stack.length;
    }

    @Override
    public int getFreeSpace()
    {
        return stack.length - stackptr;
    }
}
