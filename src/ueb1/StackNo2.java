package ueb1;

import java.util.ArrayList;

/**
 * @author falx
 * @version v0.1a
 */
public class StackNo2 implements IStack
{
    private int size = 10; // größe des Stacks
    private ArrayList<Object> list = new ArrayList<>( size );
    private int ptr = 0; // zeigt auf das nächste leere Feld


    public StackNo2( int size )
    {
        this.size = size;
    }


    @Override
    public void push( Object element ) throws StackFullException
    {
        if( ptr == size )
            throw new StackFullException(  );
        else
        {
            list.add( element );
            ptr++;
        }

    }

    @Override
    public Object pop()
    {
        if( ptr == 0 ) // abfrage des Pointers hat gefehlt, somit wurde eine ArrayListIndexOutOfBounds geworfen!
            return null;

        Object o = list.get( --ptr );
        list.remove( ptr );
        //ptr--;
        return o;
    }

    @Override
    public void increaseSize( int newSize ) throws IllegalArgumentException
    {
        if( newSize < this.size )
            throw new IllegalArgumentException( "NewSize must be greater or equals to the current size!" );
        else
            this.size = newSize;
    }

    @Override
    public int getSize()
    {
        return this.size;
    }

    @Override
    public int getFreeSpace()
    {
        return size-ptr;
    }
}
