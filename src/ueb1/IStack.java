package ueb1;

/**
 * To be written generic if already possible!
 * @author falx
 * @version v0.1a
 */
public interface IStack
{
    /**
     * Pushes one element on the top of the stack
     * @param element the element to push to the stack
     * @throws StackFullException if the stack has already reached max size
     */
    void push( Object element) throws StackFullException;

    /**
     * Pops the top element from the stack and removes it!
     * @return the top element of the stack
     */
    Object pop();

    /**
     * Increases the size of the stack. A reduce is <b>NOT</b> possible!
     * @param newSize the new size of the stack. must be greater or equal to the current size
     * @throws java.lang.IllegalArgumentException if the size is smaller than the current size
     */
    void increaseSize( int newSize ) throws IllegalArgumentException;


    /** @return the current size of the stack */
    int getSize();

    /** @return the current free space */
    int getFreeSpace();

}
