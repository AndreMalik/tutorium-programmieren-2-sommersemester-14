package ueb1;

/**
 * @author falx
 * @version v0.1a
 */
public class QueueTest
{
    public static void main( String[] args )
    {
        Queue queue = new Queue( 10 );

        enqueue( queue, 11 );

        System.out.println( "\n\n\n" );

        dequeue( queue, 11  );

        System.out.println( "\n\n\n" );

        enqueue( queue, 10 );

        System.out.println( "\n\n\n" );

        dequeue( queue, 3 );

        System.out.println( "\n\n\n" );

        enqueue( queue, 4 );

        System.out.println( "\n\n\n" );

        dequeue( queue, 10 );
    }


    private static void enqueue( Queue queue, int max )
    {
        for( int i = 0; i < max; i++ )
            System.out.printf( "\t try to enqueue %d:\t%b\n", i, queue.enqueue( i ) );
    }

    private static void dequeue( Queue queue, int max )
    {
        Object o;
        for( int i = 0; i < max; i++ )
            System.out.printf( "\t dequeue from queue: %s\n", (o = queue.dequeue()) == null ? "null" : o.toString() );
    }
}
