package ueb1;

/**
 * @author falx
 * @version v0.1a
 */
public class StackFullException extends Exception
{
    public StackFullException( String msg )
    {
        super( msg );
    }

    public StackFullException()
    {
        super();
    }
}
