package ueb3;

import java.io.*;
import java.util.HashMap;

/**
 * @author falx
 * @version v0.1a
 */

public class ConfigurationManager
{
    private final BufferedReader mBufferedReader;
    private final File mConfigFile;
    private final HashMap<String, String> mConfigMap;

    public ConfigurationManager( File configFile ) throws FileNotFoundException
    {
        this.mConfigFile = configFile;
        System.out.println( configFile.exists() );
        this.mBufferedReader = new BufferedReader( new FileReader( this.mConfigFile ) );
        this.mConfigMap = new HashMap<>();
    }

    public boolean readFile()
    {
        try
        {
            String s;
            while( (s = mBufferedReader.readLine()) != null )
            {
                s = s.trim();
                if( s.equals( "" ) )
                    continue;

                String[] confPair = s.split( ":" );
                if( confPair.length == 2 )
                    mConfigMap.put( confPair[0], confPair[1] );
            }
        }
        catch( IOException exc )
        {
            return false;
        }

        return true;
    }

    public HashMap<String, String> getConfigurationMap()
    {
        return mConfigMap;
    }


    public String get( String configKey )
    {
        return mConfigMap.get( configKey );
    }


    public void set( String configKey, String configValue )
    {
        mConfigMap.put( configKey, configValue );
    }


    public File getFile()
    {
        return mConfigFile;
    }
}
